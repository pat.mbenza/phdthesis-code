using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*[RequireComponent(typeof(Camera))]*/
public class HandRecognizer : MonoBehaviour
{
    public Camera Cam;

    private MediaPipeClient _mpClient1;
    //private Visualizer _mpHandDrawer;

    public Canvas canvas;
    public RawImage rawImage;

    public GameObject landmark_2D;

    void Awake()
    {
       
        _mpClient1 = new MediaPipeClient();
        // _mpHandDrawer = gameObject.GetComponent<Visualizer>();
        this._mpClient1.PNGData = new byte[] { };


    }

    public void CallTakeSnapshot()
    {
        //snapCam.gameObject.SetActive(true);

    }

    private void Start()
    {
        _mpClient1.Start();
    }

    private void OnDestroy()
    {
        //Debug.Log("Recognizer destroyed! Thread about to be stopped!");
        //_mpClient1.Stop();
    }

    void LateUpdate()
    {

        //Debug.Log("_mpClient1.isAlive(): " + _mpClient1.isAlive());
        if (_mpClient1.FrameCount >= 300)
        {

            _mpClient1.Stop();
            MediaPipeClient _mpClient2 = new MediaPipeClient();
            // _mpHandDrawer = gameObject.GetComponent<Visualizer>();
            _mpClient2.PNGData = new byte[] { };

            _mpClient2.Start();

            // _mpClient1.Start();
            this._mpClient1 = _mpClient2;

        }

    }

    public void FetchLandmarks(byte[] bytes, int width, int height)
    {

        this._mpClient1.PNGData = bytes;

        if ((this._mpClient1.PNGData is null) && (this._mpClient1.PNGData.Length == 0))
            Debug.Log("BYTES EMPTY-Fetch");

        this._mpClient1.imgWidth = width;
        this._mpClient1.imgHeight = height;

        

        if (this._mpClient1.msg != "")
        {
            MP_Result results = JsonUtility.FromJson<MP_Result>(this._mpClient1.msg);
           // Debug.Log(_mpHandDrawer);
            foreach (MP_Hand hand in results.mP_Hands)
            {
               DrawHand(hand.landmarks, this._mpClient1.imgWidth, this._mpClient1.imgHeight);//left
            }

        }

    }

    //Instantiates Landmark GOs to given coords
    internal void DrawHand(List<MP_Landmark> landmarks, int width, int height)
    {
        foreach (MP_Landmark lm in landmarks)
        {

            Debug.Log("Point: "+ lm.point);
            //convertir par rapport au ScreenSpace
            Vector3 positionOnCanvas = Vector3.Scale(lm.normalizedLandmark, new Vector3(Screen.width, Screen.height, 1));
            
            //projeter pixel coordinates sur un plan (canvas)
            positionOnCanvas = Cam.ScreenToWorldPoint(
                                                 new Vector3(
                                                                 positionOnCanvas.x,
                                                                 positionOnCanvas.y,
                                                                 canvas.planeDistance
                                                 )
                                        );

            GameObject ld = Instantiate(landmark_2D, positionOnCanvas, Quaternion.identity);

            ld.transform.position = Vector3.Scale(ld.transform.position, new Vector3(-1, -1, 1));

            Destroy(ld, 0.12f);
            //this._mpClient1.msg = "";

        }
    }


    //Moves existing landmark GOs to the given coords
    /*
    internal void DrawHand(List<MP_Landmark> landmarks, int width, int height)
    {
        foreach (MP_Landmark lm in landmarks)
        {

            //Instantiate at position (0, 0, 0) and zero rotation.
            Debug.Log("pixel coords:" + lm.pixelCoordinates);
            GameObject ld = GameObject.Find(lm.point+"");
            Debug.Log("lm.point: " + lm.point);
            Debug.Log("ld.name: " + ld.name);

            
            ld.transform.localScale = new Vector3(0.1f, 0.1f, 1f);
            ld.GetComponent<RectTransform>().SetParent(canvas.transform);
            ld.transform.localPosition *= -1;
            //Destroy(ld, 0.20f);
            

        }
    }
   */

    //Places LandMarks in the Camera's (as children) local space
    /*
    internal void DrawHand(List<MP_Landmark> landmarks, int width, int height)
    {
        foreach (MP_Landmark lm in landmarks)
        {
            
            Debug.Log("pixel coords:" + lm.pixelCoordinates);

            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            sphere.transform.position = new Vector3(0, 0, 8); // valeurs arbitraies de positionnement rapprocher du plan

           // sphere.transform.SetParent(Cam.transform);

            sphere.transform.position += lm.normalizedLandmark;

            //sphere.transform.position *= Cam.pixelWidth;

            sphere.transform.position = Vector3.Scale(sphere.transform.position, new Vector3(-8, -8, 1)); // valeurs arbitraies de positionnement

            

            sphere.transform.localPosition += new Vector3(2, 5, 0);

            sphere.transform.localScale *= 0.25f;

            //sphere.transform.localPosition *= -1;

            Destroy(sphere, 0.09f);
        }
    }
    */

    Rect NearPlaneDimensions(Camera cam)
    {
        Rect r = new Rect();
        float a = cam.nearClipPlane;//get length
        float A = cam.fieldOfView * 0.5f;//get angle
        A = A * Mathf.Deg2Rad;//convert tor radians
        float h = (Mathf.Tan(A) * a);//calc height
        float w = (h / cam.pixelHeight) * cam.pixelWidth;//deduct width

        r.xMin = -w;
        r.xMax = w;
        r.yMin = -h;
        r.yMax = h;

        Debug.Log("NearPlaneDimensions:" + r.width+ " , " + r.height);
        Debug.Log("NearPlaneDimensions_2:" + r.xMax + " , " + r.yMax);

        return r;
    }
}
