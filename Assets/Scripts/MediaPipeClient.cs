﻿using AsyncIO;
using NetMQ;
using NetMQ.Sockets;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
///     Example of requester who only sends Hello. Very nice guy.
///     You can copy this class and modify Run() to suits your needs.
///     To use this class, you just instantiate, call Start() when you want to start and Stop() when you want to stop.
/// </summary>
public class MediaPipeClient : RunAbleThread
{
  
    //public string msg = "";
    public string msg = "{\"mP_Hands\":[{\"index\":1,\"score\":0.999995,\"label\":\"Right\",\"landmarks\":[{\"point\":0,\"pixelCoordinates\":{\"x\":585,\"y\":358},\"normalizedLandmark\":{\"x\": 0.69089323, \"y\": 0.74751997, \"z\": -4.922519e-05}},{\"point\":1,\"pixelCoordinates\":{\"x\":545,\"y\":335},\"normalizedLandmark\":{\"x\": 0.64364296, \"y\": 0.698781, \"z\": -0.02811876}},{\"point\":2,\"pixelCoordinates\":{\"x\":525,\"y\":295},\"normalizedLandmark\":{\"x\": 0.6195207, \"y\": 0.6159005, \"z\": -0.041243758}},{\"point\":3,\"pixelCoordinates\":{\"x\":517,\"y\":263},\"normalizedLandmark\":{\"x\": 0.61066437, \"y\": 0.5493843, \"z\": -0.05704338}},{\"point\":4,\"pixelCoordinates\":{\"x\":504,\"y\":235},\"normalizedLandmark\":{\"x\": 0.59520763, \"y\": 0.4906028, \"z\": -0.07326847}},{\"point\":5,\"pixelCoordinates\":{\"x\":563,\"y\":248},\"normalizedLandmark\":{\"x\": 0.66479814, \"y\": 0.517913, \"z\": -0.0012455365}},{\"point\":6,\"pixelCoordinates\":{\"x\":565,\"y\":204},\"normalizedLandmark\":{\"x\": 0.66643536, \"y\": 0.42707905, \"z\": -0.015553824}},{\"point\":7,\"pixelCoordinates\":{\"x\":566,\"y\":178},\"normalizedLandmark\":{\"x\": 0.6679896, \"y\": 0.3715818, \"z\": -0.023937924}},{\"point\":8,\"pixelCoordinates\":{\"x\":568,\"y\":151},\"normalizedLandmark\":{\"x\": 0.67072225, \"y\": 0.31641084, \"z\": -0.02320918}},{\"point\":9,\"pixelCoordinates\":{\"x\":590,\"y\":253},\"normalizedLandmark\":{\"x\": 0.6967513, \"y\": 0.5291017, \"z\": -0.0058977017}},{\"point\":10,\"pixelCoordinates\":{\"x\":597,\"y\":213},\"normalizedLandmark\":{\"x\": 0.7043871, \"y\": 0.4457636, \"z\": -0.035158925}},{\"point\":11,\"pixelCoordinates\":{\"x\":601,\"y\":191},\"normalizedLandmark\":{\"x\": 0.7089674, \"y\": 0.39809704, \"z\": -0.06625843}},{\"point\":12,\"pixelCoordinates\":{\"x\":603,\"y\":169},\"normalizedLandmark\":{\"x\": 0.7122316, \"y\": 0.35212722, \"z\": -0.08237072}},{\"point\":13,\"pixelCoordinates\":{\"x\":614,\"y\":262},\"normalizedLandmark\":{\"x\": 0.72522944, \"y\": 0.54783964, \"z\": -0.016764151}},{\"point\":14,\"pixelCoordinates\":{\"x\":615,\"y\":250},\"normalizedLandmark\":{\"x\": 0.7256109, \"y\": 0.52226216, \"z\": -0.062036723}},{\"point\":15,\"pixelCoordinates\":{\"x\":601,\"y\":279},\"normalizedLandmark\":{\"x\": 0.7097822, \"y\": 0.5830118, \"z\": -0.07718531}},{\"point\":16,\"pixelCoordinates\":{\"x\":592,\"y\":300},\"normalizedLandmark\":{\"x\": 0.6985672, \"y\": 0.62626624, \"z\": -0.07117893}},{\"point\":17,\"pixelCoordinates\":{\"x\":637,\"y\":275},\"normalizedLandmark\":{\"x\": 0.7522534, \"y\": 0.5736384, \"z\": -0.029146194}},{\"point\":18,\"pixelCoordinates\":{\"x\":647,\"y\":241},\"normalizedLandmark\":{\"x\": 0.76309836, \"y\": 0.5034698, \"z\": -0.03797012}},{\"point\":19,\"pixelCoordinates\":{\"x\":650,\"y\":217},\"normalizedLandmark\":{\"x\": 0.76717955, \"y\": 0.45266134, \"z\": -0.04191515}},{\"point\":20,\"pixelCoordinates\":{\"x\":654,\"y\":193},\"normalizedLandmark\":{\"x\": 0.7716222, \"y\": 0.40295503, \"z\": -0.043652248}}]}]}";
    public byte[] PNGData;
    public int imgWidth;
    public int imgHeight;
    //public string endpointAdress = "tcp://4.tcp.eu.ngrok.io:19929";
    public string endpointAdress = "tcp://5.tcp.eu.ngrok.io:10811";
    public int FrameCount = 0;

    


    ///--->protected new bool Running = false; ///--->

    /// <summary>
    ///     Request Hello message to server and receive message back. Do it 10 times.
    ///     Stop requesting when Running=false.
    /// </summary>
    protected override void Run()
    {
        ForceDotNet.Force(); // this line is needed to prevent unity freeze after one use, not sure why yet
        using (RequestSocket client = new RequestSocket())
        {    
            client.Connect(endpointAdress);/////->
            //for (int i = 0; i < 1000 && Running; i++)//C'EST ICI LA LIMITE A 10 !!!
            while (FrameCount < 300 && Running)
            //while (Running)
            {
                Debug.Log("Still Running!");
                //client.SendFrame("Hello");
                // ReceiveFrameString() blocks the thread until you receive the string, but TryReceiveFrameString()
                // do not block the thread, you can try commenting one and see what the other does, try to reason why
                // unity freezes when you use ReceiveFrameString() and play and stop the scene without running the server
                //                string message = client.ReceiveFrameString();
                //                Debug.Log("Received: " + message);


                Debug.Log("PNGData is null:" + !(PNGData is null));
                Debug.Log("(PNGData.Length != 0:" + (PNGData.Length != 0));
                if (!(PNGData is null) && (PNGData.Length != 0))
                {

                    client.SendFrame(PNGData);
                    FrameCount++;
                    Debug.Log("FrameCount:" + FrameCount);
                    //}
                    //else 
                    //{
                    //  Debug.Log("BYTES EMPTY-Client");
                    //}

                    string message = null;
                    bool gotMessage = false;
                    //while (Running && !(PNGData is null) && (PNGData.Length != 0))
                    while (Running)
                    {
                        Debug.Log("Should be the last message:");
                        gotMessage = client.TryReceiveFrameString(out message); // this returns true if it's successful
                        Debug.Log("After TryReceive");
                        if (gotMessage) break;
                    }

                    if (gotMessage)
                    {
                        //Debug.Log("Received " + message);
                        //set UI text
                        this.msg = message;
                        //this.msg = this.msg.Replace("\"","\\\"");
                        Debug.Log("Received " + msg);

                    }
                }

            }
           /* if (FrameCount == 300)
            {
                
                this.Start();

                this.FrameCount = 0;
            }*/
        }

        NetMQConfig.Cleanup(); // this line is needed to prevent unity freeze after one use, not sure why yet
    }

    public Boolean isAlive() 
    {
        return Running;
    }

}