﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class MP_Result
{
    //public List<MP_Hands> mP_Hands;
    public MP_Hand[] mP_Hands;
}


[System.Serializable]
public class MP_Hand 
{
    public int index;
    public float score;
    public string label;
    //public bool Active { get; set; }
    //public DateTime CreatedDate { get; set; }
    public List<MP_Landmark> landmarks;
  
}

[System.Serializable]
public class MP_Landmark
{
    public int point;
    public Vector2 pixelCoordinates;
    public Vector3 normalizedLandmark;
}
