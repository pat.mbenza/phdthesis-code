using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameCapture : MonoBehaviour
{
    static WebCamTexture webCamTexture;
    private int _CaptureCounter = 0;
    public HandRecognizer recognizer;
    //private bool SkipThisFrame = false;
    private int SkipEveryXFrame = 2;
    private int FrameCounter = 1;

 
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            TakeSnapShotOfWebCam();
        }

        ////if (Input.GetKeyDown(KeyCode.F))
        ////{
           //if (SkipThisFrame)
           //{
        if ((FrameCounter % SkipEveryXFrame) > 0f)
        {
            TransferBytes(CaptureFrameFromWebCam());
            FrameCounter++;
            FrameCounter = FrameCounter % 1000;
        }
           //}

           //SkipThisFrame = !SkipThisFrame;
            //}
    }

    void TakeSnapShotOfWebCam()
    {
        webCamTexture = (WebCamTexture)GetComponent<RawImage>().texture;
       
        Texture2D snap = new Texture2D(webCamTexture.width, webCamTexture.height);

        snap.SetPixels(webCamTexture.GetPixels());

        snap.Apply();

        ////-->byte[] bytes = snap.EncodeToPNG();
        byte[] bytes = snap.EncodeToPNG();

        System.IO.File.WriteAllBytes("Assets/Snapshots/" + _CaptureCounter.ToString() + ".png", bytes);
        ++_CaptureCounter;

        Debug.Log("Snapshot taken!");

    }

    byte[] CaptureFrameFromWebCam()
    {

        webCamTexture = (WebCamTexture)GetComponent<RawImage>().texture;

        //if (!(webCamTexture is null))

        Texture2D snap = new Texture2D(webCamTexture.width, webCamTexture.height);

        snap.SetPixels(webCamTexture.GetPixels());

        snap.Apply();

        byte[] bytes = snap.EncodeToPNG();


        if ((bytes is null) && (bytes.Length == 0))
            Debug.Log("BYTES EMPTY-Capture");

        return bytes;

    }

    void TransferBytes(byte[] bytes) {

        recognizer.FetchLandmarks(bytes, webCamTexture.width, webCamTexture.height);
        //Debug.Log("Fetch :" + bytes.Length);
        //Debug.Log("Bytes is null :" + (bytes is null));
        if ((bytes is null) && (bytes.Length == 0))
            Debug.Log("BYTES EMPTY-Transfer");

    }

}
