## Provisional title

Introducing A New Paradigm For Hand Gesture-Based Interaction For AR/MR

## Background

The [CardiAmmonia](https://www.polemecatech.be/en/projets/cardiammonia/) Project aims to bring, in the PET cardiac market, a compact, cost-effective, and easy-to-use solution for 13N-Ammonia production. The solution requires the development of a supervision software to coordinate the whole workflow by integrating the control of the different hardware elements—thus allowing a PET technologist to operate the full system.

In addition to the innovative aspect of the global solution, each hardware and software sub-system requires some innovative developments too, in order to achieve the key goals (user-friendliness & cost-effectiveness).

The project is also described as … "a first step into the IBA RPS strategy to have a "dose on-demand" solution for oncology tracers (which require a more complex chemistry process and QC protocol), especially for emerging countries."


## Aims

Augmented/Mixed Reality  Maintenance Systems provide adaptive support and feedback to a user during maintenance operations by overlaying virtual instructions and data onto the equipment they’re working on.

For example, looking at a piece of equipment, like an engine, from an AR/MR headset could reveal an overlay of critical information about it, such as its current performance, output, and temperature, as well as an indication of the area where a failure occurred, along with repair instructions.

**The purpose of this project is to research how 3D hand-gesture recognition can be used to improve the Naturalness of AR-assisted maintenance.** 

By tracking the users’ hands via a depth or RGB-D camera, recognizing their gestures in a first person view, and then combining it with other modalities (such as Eye tracking,  Noise Detection, Object Tracking, etc...), we aim to introduce an unobtrusive interaction metaphor for AR guidance that improves the usability, user satisfaction, and efficiency of existing solutions. While also taking into account the short and long term objectives of the CardiAmmonia project (ease of use, cost-effectiveness, emerging countries).

## Demo
A Demo of this project can be found [here](https://www.youtube.com/watch?v=VJXyxAfRP8I)